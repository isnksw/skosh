# Skosh - A tiny client-side CMS/Wiki system

Skosh is a tiny SMS/Wiki system written in HTML5 and Javascript runs on completely client-side.

## Documentation/Sample site
https://isnksw.gitlab.io/skosh/ (Japanese)

## Download
https://gitlab.com/isnksw/skosh/-/archive/master/skosh-master.zip?path=src

## Source Code
https://gitlab.com/isnksw/skosh

## License
&copy; 2019 Isana Kashiwai ([MIT license](https://gitlab.com/isnksw/skosh/blob/master/LICENSE))

## Copyrights
Skosh depends on below superb libraries.
- [markdown-it.js](https://github.com/markdown-it/markdown-it) ([MIT License](https://github.com/markdown-it/markdown-it/blob/master/LICENSE))
- [markdown-it-footnote](https://github.com//markdown-it/markdown-it-footnote) ([MIT License](https://github.com/markdown-it/markdown-it-footnote/blob/master/LICENSE))
- [markdown-it-task-lists.js](https://github.com/revin/markdown-it-task-lists) ([ISC License](https://github.com/revin/markdown-it-task-lists/blob/master/LICENSE))
- [markdown-it-attrs](https://github.com/arve0/markdown-it-attrs)([MIT License](https://github.com/arve0/markdown-it-attrs/blob/master/LICENSE))
- [DOMPurify](https://github.com/cure53/DOMPurify)([Apache/Mozilla Public License](https://github.com/cure53/DOMPurify/blob/master/LICENSE))
- [highlight.js](https://highlightjs.org/) ([BSD License](https://github.com/highlightjs/highlight.js/blob/master/LICENSE))
- [MathJax](https://www.mathjax.org/) ([Apache License 2.0](https://github.com/mathjax/MathJax/blob/master/LICENSE))
- [c3.js](https://c3js.org/) ([The MIT License](https://github.com/c3js/c3/blob/master/LICENSE))
- [flowchart.js](https://flowchart.js.org/) ([MIT License](https://github.com/adrai/flowchart.js/blob/master/license))
- [js-sequence-diagrams](https://bramp.github.io/js-sequence-diagrams/) ([BSD 2-Clause "Simplified" License](https://github.com/bramp/js-sequence-diagrams/blob/master/LICENCE))