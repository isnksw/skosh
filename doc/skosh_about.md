# Skosh: About

Skoshは、Markdownで書いたファイルを読み込んで、レンダリングして、表示するだけのシンプルなCMSです。ブラウザ上のJavaScriptだけで動作し、静的なファイルのみで構成されているので、必要なファイルをアップロードするだけで動きます。ローカルにもサーバ側にも何かをインストールしたり、設定したりする必要がありません。

toc{.toc}

## Skoshの特徴

**導入が簡単:**
サーバに必要なファイルをアップロードするだけで動きます。導入にあたって、何かをインストールしたり、設定したりする必要がありません。

**更新が簡単:**
Markdownでマークアップされたテキストファイルをアップロードするだけで、それっぽくレイアウトされたページが表示されます。

**機能拡張が簡単:**
本体の機能を極力抑え、Markdownのレンダリングやページのレイアウトのプロセスに介入するAPIを用意することで、JavaScriptによる機能追加、プラグインの開発などがしやすくなっています。

## Skoshにできないこと
一方で、以下のようなことには向いていません

**サーバ上でページを作成・編集することはできません:**
静的ファイルのみで構成されているため、サーバ上で動作するフルスペックのCMSのような、ページの作成・編集システムはありません。別途エディタなどで作成・編集したファイルをアップロードする必要があります。

**全文検索・タグ/カテゴリによる分類などはできません:**
指定されたMarkdownファイルをレイアウトして表示する機能しか持たないため、ページ間をまたいだ検索や、タグ・カテゴリによる分類などはできません。

**ページの閲覧及び内容の取得にJavaScriptが必要です:**
ページのコンテンツをJavaScriptで動的に生成しているため、JavaScriptの使えない環境ではページを閲覧できません。また、検索エンジンでのインデックス化やページクリッピングサービスなどでコンテンツが取得できないなどの問題が生じる可能性があります。

## Download
https://gitlab.com/isnksw/skosh/-/archive/master/skosh-master.zip?path=src

## License
&copy; 2019 Isana Kashiwai ([MIT license](https://gitlab.com/isnksw/skosh/blob/master/LICENSE))

## Copyrights
Skoshおよび同包のプラグインは以下のライブラリを使用しています。

### 本体
- [markdown-it.js](https://github.com/markdown-it/markdown-it) ([MIT License](https://github.com/markdown-it/markdown-it/blob/master/LICENSE))
- [markdown-it-footnote](https://github.com//markdown-it/markdown-it-footnote) ([MIT License](https://github.com/markdown-it/markdown-it-footnote/blob/master/LICENSE))
- [markdown-it-task-lists.js](https://github.com/revin/markdown-it-task-lists) ([ISC License](https://github.com/revin/markdown-it-task-lists/blob/master/LICENSE))
- [markdown-it-attrs](https://github.com/arve0/markdown-it-attrs)([MIT License](https://github.com/arve0/markdown-it-attrs/blob/master/LICENSE))
- [DOMPurify](https://github.com/cure53/DOMPurify)([Apache/Mozilla Public License](https://github.com/cure53/DOMPurify/blob/master/LICENSE))

### プラグイン
- [highlight.js](https://highlightjs.org/) ([BSD License](https://github.com/highlightjs/highlight.js/blob/master/LICENSE))
- [MathJax](https://www.mathjax.org/) ([Apache License 2.0](https://github.com/mathjax/MathJax/blob/master/LICENSE))
- [c3.js](https://c3js.org/) ([The MIT License](https://github.com/c3js/c3/blob/master/LICENSE))
- [flowchart.js](https://flowchart.js.org/) ([MIT License](https://github.com/adrai/flowchart.js/blob/master/license))
- [js-sequence-diagrams](https://bramp.github.io/js-sequence-diagrams/) ([BSD 2-Clause "Simplified" License](https://github.com/bramp/js-sequence-diagrams/blob/master/LICENCE))