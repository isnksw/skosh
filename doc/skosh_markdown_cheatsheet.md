
# Skosh: Markdown CheatSheet

TOC{.toc}

## Common Markdown
標準的なMarkdown記法はほぼ全て使えます。レイアウトは、すべて`/assets/css/default.css`内で行っています。

HTMLへの変換には [markdown-it](https://github.com/markdown-it/markdown-it)を使用しています。

### Headers
```
# H1
## H2
### H3
#### H4
##### H5
###### H6

Alternatively, for H1 and H2, an underline-ish style:

Alt-H1
======

Alt-H2
------
```


### Emphasis
```
Emphasis, aka italics, with *asterisks* or _underscores_.
Strong emphasis, aka bold, with **asterisks** or __underscores__.
Combined emphasis with **asterisks and _underscores_**.
Strikethrough uses two tildes. ~~Scratch this.~~
```
Emphasis, aka italics, with *asterisks* or _underscores_.
Strong emphasis, aka bold, with **asterisks** or __underscores__.
Combined emphasis with **asterisks and _underscores_**.
Strikethrough uses two tildes. ~~Scratch this.~~

### List

(In this example, leading and trailing spaces are shown with with dots: ⋅)

```
1. First ordered list item
2. Another item
⋅⋅* Unordered sub-list.
1. Actual numbers don't matter, just that it's a number
⋅⋅1. Ordered sub-list
4. And another item.

⋅⋅⋅You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we'll use three here to also align the raw Markdown).

⋅⋅⋅To have a line break without a paragraph, you will need to use two trailing spaces.⋅⋅
⋅⋅⋅Note that this line is separate, but within the same paragraph.⋅⋅
⋅⋅⋅(This is contrary to the typical GFM line break behaviour, where trailing spaces are not required.)

* Unordered list can use asterisks
- Or minuses
+ Or pluses
```
(In this example, leading and trailing spaces are shown with with dots: ⋅)

1. First ordered list item
2. Another item
  * Unordered sub-list.
1. Actual numbers don't matter, just that it's a number
  1. Ordered sub-list
4. And another item.

   You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we'll use three here to also align the raw Markdown).

   To have a line break without a paragraph, you will need to use two trailing spaces.⋅⋅
   Note that this line is separate, but within the same paragraph.⋅⋅
   (This is contrary to the typical GFM line break behaviour, where trailing spaces are not required.)

* Unordered list can use asterisks
- Or minuses
+ Or pluses


### Links
There are two ways to create links.
```
[I'm an inline-style link](https://www.google.com)

[I'm a reference-style link][Arbitrary case-insensitive reference text]

[You can use numbers for reference-style link definitions][1]

Or leave it empty and use the [link text itself]

URLs and URLs in angle brackets will automatically get turned into links.
http://www.example.com or <http://www.example.com> and sometimes
example.com (but not on Github, for example).

Some text to show that the reference links can follow later.

[arbitrary case-insensitive reference text]: https://www.mozilla.org
[1]: http://slashdot.org
[link text itself]: http://www.reddit.com
```

[I'm an inline-style link](https://www.google.com)

[I'm a reference-style link][Arbitrary case-insensitive reference text]

[You can use numbers for reference-style link definitions][1]

Or leave it empty and use the [link text itself]

URLs and URLs in angle brackets will automatically get turned into links.
http://www.example.com or <http://www.example.com> and sometimes
example.com (but not on Github, for example).

Some text to show that the reference links can follow later.

[arbitrary case-insensitive reference text]: https://www.mozilla.org
[1]: http://slashdot.org
[link text itself]: http://www.reddit.com


### Images
```
Here's our logo (hover to see the title text):

Inline-style:
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")

Reference-style:
![alt text][logo]

[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2"
```
Here's our logo (hover to see the title text):

Inline-style:
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")

Reference-style:
![alt text][logo]

[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2"


### Code
```
Inline `code` has `back-ticks around` it.
```
Inline `code` has `back-ticks around` it.


Blocks of code are either fenced by lines with three back-ticks ```, or are indented with four spaces.

    ```javascript
    var s = "Toto, I've a feeling we're not in Kansas anymore.";
    alert(s);
    ```
```
var s = "Toto, I've a feeling we're not in Kansas anymore.";
alert(s);
```
#### Code Highlight (highlight.js / WispHighlight Extension)
    ```javascript
    var s = "Toto, I've a feeling we're not in Kansas anymore.";
    alert(s);
    ```

```javascript
var s = "Toto, I've a feeling we're not in Kansas anymore.";
alert(s);
```


### Tables
```
Colons can be used to align columns.

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

The outer pipes (|) are optional, and you don't need to make the raw Markdown line up prettily. You can also use inline Markdown.

Markdown | Less | Pretty
--- | --- | ---
*Still* | `renders` | **nicely**
1 | 2 | 3
```
Colons can be used to align columns.

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

The outer pipes (|) are optional, and you don't need to make the raw Markdown line up prettily. You can also use inline Markdown.

Markdown | Less | Pretty
--- | --- | ---
*Still* | `renders` | **nicely**
1 | 2 | 3

### Blockquote
```
> Blockquotes are very handy in email to emulate reply text.
> This line is part of the same quote.

Quote break.

> This is a very long line that will still be quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote.
```
> Blockquotes are very handy in email to emulate reply text.
> This line is part of the same quote.

Quote break.

> This is a very long line that will still be quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote.


### Inline HTML
```
<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. Use HTML <em>tags</em>.</dd>
</dl>

H<sub2<sub>O

100m<sup>2</sup>

```
<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. Use HTML <em>tags</em>.</dd>
</dl>

H<sub>2</sub>O

100m<sup>2</sup>

### Horizontal Rule
```
Three or more...

---

Hyphens

***

Asterisks

___

Underscores
```
Three or more...

---

Hyphens

***

Asterisks

___

Underscores


### Line Breaks

My basic recommendation for learning how line breaks work is to experiment and discover -- hit <Enter> once (i.e., insert one newline), then hit it twice (i.e., insert two newlines), see what happens. You'll soon learn to get what you want. "Markdown Toggle" is your friend.

Here are some things to try out:
```
Here's a line for us to start with.

This line is separated from the one above by two newlines, so it will be a *separate paragraph*.

This line is also a separate paragraph, but...
This line is only separated by a single newline, so it's a separate line in the *same paragraph*.
```
Here's a line for us to start with.

This line is separated from the one above by two newlines, so it will be a separate paragraph.

This line is also begins a separate paragraph, but...
This line is only separated by a single newline, so it's a separate line in the same paragraph.

(Technical note: Markdown Here uses GFM line breaks, so there's no need to use MD's two-space line breaks.)

## 拡張記法
デフォルトで、以下のMarkdown拡張記法に対応しています。以下の記法は他のシステムでは動作しない場合があります。

### 脚注
MarkdownのPandoc拡張記法で、脚注の表示ができます。
```
こんな感じで[^1]、脚注の表示ができます^[行内に書くパターン]。

[^1]:脚注部分を別行に書くパターン。
```

こんな感じで[^1]、脚注の表示ができます^[行内に書くパターン]。

[^1]:脚注部分を別行に書くパターン。

表示には[markdown-it-footnote](https://github.com//markdown-it/markdown-it-footnote)を利用しています。


### タスクリスト
MarkdownのGithub拡張記法で、タスクリスト(チェックボックス)の表示ができます。

```
- [x] 項目1
- [ ] 項目2
  - [ ] 項目2-2
```

- [x] 項目1
- [ ] 項目2
  - [ ] 項目2-2

表示には[markdown-it-task-lists](https://github.com/revin/markdown-it-task-lists)を利用しています。

### 属性値
MarkdownのPandoc拡張記法で、HTMLの属性値の指定ができます。ただし、スクリプトを動作させるような *危険な* 属性は無視されます（[markdown-it-attrs](https://github.com/arve0/markdown-it-attrs)の標準の動作です）。

```
クラスを指定{.custom_class}

スタイルを直接指定{style="color:red"}

ただし、一部の属性は無視されます。{onmouseover="alert('alert!')"}

```

クラスを指定{.custom_class}

スタイルを直接指定{style="color:red"}

ただし、デフォルトではスクリプトを動作させるような属性は無視されます{onmouseover="alert('alert!')"}

表示には[markdown-it-attrs](https://github.com/arve0/markdown-it-attrs)を利用しています。

## Skosh独自拡張記法
以下は、Skoshの独自拡張です。以下の記法は他のシステムでは動作しません。

### 目次
CSSで`toc`というクラスを指定すると、該当箇所にページ内の見出しタグ（h2〜h3）をリストにして目次として表示します。このページの冒頭部分で使用しています。

Markdownの拡張記法を使って、以下のようにclassを指定するか(この場合TOCの文字は無視されます)、

```
TOC{.toc}
```

あるいはHTMLを直接書いても同じ動作になります。

```
<span class="toc"></span>
```

### 数式

```
#### ケプラーの第1法則:
$$ r = \frac{a^2/b}{1 + \epsilon \cos \theta} \tag{1}$$
\\( a \\)は軌道長半径、\\( b \\)は軌道短半径、\\( \epsilon \\)は離心率、\\(\theta\\)は真近点角  

#### ケプラーの第2法則:
$$ \frac{1}{2} r^2 \frac{d\theta}{dt} = \frac{dA}{dt} \tag{2}$$

#### ケプラーの第3法則:
$$ \frac{T^2}{a^3} = constant \tag{3}$$

#### ケプラー方程式:
$$ M = E-\epsilon \sin E \tag{4}$$
```

#### ケプラーの第1法則:
$$ r = \frac{a^2/b}{1 + \epsilon \cos \theta} \tag{1}$$
\\( a \\)は軌道長半径、\\( b \\)は軌道短半径、\\( \epsilon \\)は離心率、\\(\theta\\)は真近点角  

#### ケプラーの第2法則:
$$ \frac{1}{2} r^2 \frac{d\theta}{dt} = \frac{dA}{dt} \tag{2}$$

#### ケプラーの第3法則:
$$ \frac{T^2}{a^3} = constant \tag{3}$$

#### ケプラー方程式:
$$ M = E-\epsilon \sin E \tag{4}$$

動的に読み込まれた...(略)

### グラフ

cf. [c3.js](https://c3js.org/)

    ```chart
    {
        "data": {
        "columns": [
            ["data1", 30, 200, 100, 400, 150, 250],
            ["data2", 50, 20, 10, 40, 15, 25]
        ]
        }
    }
    ```

```chart
{
    "data": {
    "columns": [
        ["data1", 30, 200, 100, 400, 150, 250],
        ["data2", 50, 20, 10, 40, 15, 25]
    ]
    }
}
```

### フローチャート

cf. [flowchart.js](http://flowchart.js.org/)

    ```flowchart
    st=>start: 開始
    e=>end: 終了
    op1=>operation: 処理
    sub1=>subroutine: サブルーチン
    cond=>condition: 判断
    Yes or No?
    io=>inputoutput: 入力
    para=>parallel: 並行処理
    st->op1->cond
    
    cond(yes)->io->e
    cond(no)->para
    para(path1, bottom)->sub1(right)->op1
    para(path2, top)->op1
    ```

```flowchart
  st=>start: 開始
  e=>end: 終了
  op1=>operation: 処理
  sub1=>subroutine: サブルーチン
  cond=>condition: 判断
  Yes or No?
  io=>inputoutput: 入力
  para=>parallel: 並行処理
  st->op1->cond
  
  cond(yes)->io->e
  cond(no)->para
  para(path1, bottom)->sub1(right)->op1
  para(path2, top)->op1

```

### シーケンスダイアグラム

cf. [js-sequence-diagrams](https://bramp.github.io/js-sequence-diagrams/)

    ```diagram
    Title: Here is a title
    A->B: Normal line
    B-->C: Dashed line
    C->>D: Open arrow
    D-->>A: Dashed open arrow
    ```

```diagram
Title: Here is a title
A->B: Normal line
B-->C: Dashed line
C->>D: Open arrow
D-->>A: Dashed open arrow
```

### JavaScriptの実行
ファイルに触れるのが信頼できるユーザーだけで、不特定多数がMarkdownをアップロードすることがないなら、セキュリティオプションを外してMarkdown内のJavaScriptを実行できるようにできます^[デフォルトではMarkdownは[DOMPurify](https://github.com/cure53/DOMPurify)を通してサニタイズしているので`<script>`タグは使えません。]。

#### オプションの変更

`index.html` の以下の箇所を変更します。

```javascript
// init main
const main = new Skosh("main", {
  sanitize: "true"
});
```

`sanitize: "true"` →  `sanitize: "false"`

```javascript
// init main
const main = new Skosh("main", {
  sanitize: "false"
});
```

#### サンプル

```html
<script src="https://cdn.jsdelivr.net/gh/lizard-isana/orb.js@2.2/build/orb.v2.js"></script>
<script>
  const mars = new Orb.VSOP("Mars");
  const location = {
    "latitude":35.658,
    "longitude":139.741,
    "altitude":0
  }
  const observation = new fOrb.Observation({"observer":location,"target":mars});
  setInterval(()=>{
    let date = new Date();
    let horizontal = observation.azel(date);
    let str = `Azimuth: ${horizontal.azimuth.toFixed(5)}<br>Elevation: ${horizontal.elevation.toFixed(5)}`
    document.getElementById("orbjs_test").innerHTML= str;
  },1000)
</script>
**Output**:
<span id="orbjs_test"></span>
```
上のコードを引用符なしでそのまま書くとこうなります↓

<script src="https://cdn.jsdelivr.net/gh/lizard-isana/orb.js@2.2/build/orb.v2.js"></script>
<script>
  const mars = new Orb.VSOP("Mars");
  const location = {
    "latitude":35.658,
    "longitude":139.741,
    "altitude":0
  }
  const observation = new Orb.Observation({"observer":location,"target":mars});
  setInterval(()=>{
    let date = new Date();
    let horizontal = observation.azel(date);
    let str = `Azimuth: ${horizontal.azimuth.toFixed(5)}<br>Elevation: ${horizontal.elevation.toFixed(5)}`
    document.getElementById("orbjs_test").innerHTML= str;
  },1000)
</script>
**Output**:
<span id="orbjs_test"></span>
