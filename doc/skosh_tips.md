# Skosh:Tips

<div class="toc"></div>

## 簡易サーバを使ってローカルで動作させる
Skoshの動作にはWebサーバが必要ですが、ブラウザやエディタのプラグイン、スタンドアローンの簡易サーバアプリ、開発言語の簡易サーバなどを使うと、比較的簡単にローカルでも動作させることができます。

### ブラウザ のプラグインを使う

#### Chrome
- [Web Server for Chrome - Chrome ウェブストア](https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb?hl=ja)

### Editor のプラグインを使う
- Visual Studio Code: [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
- Atom: [atom-live-server](https://atom.io/packages/atom-live-server)
- Brackets: Live Preview
- Sublime Text: [View In Browser](http://adampresley.github.io/sublime-view-in-browser/)

### アプリケーション形式の簡易サーバを使う
- [Servez | servez](https://greggman.github.io/servez/)
- [civetweb/civetweb: Embedded C/C++ web server](https://github.com/civetweb/civetweb)

### 開発言語の簡易サーバを使う

#### Python
まずバージョンをチェック

    python -V

バージョン3.0以上なら

    python -m http.server 8080

バージョン3.0以下なら

    python -m SimpleHTTPServer 8080

#### PHP

    php -S localhost:8080

#### Ruby
    ruby -rwebrick -e 'WEBrick::HTTPServer.new(:DocumentRoot => "./", :Port => 8080).start'

#### node.js
[node-static - npm](https://www.npmjs.com/package/node-static)

nodeが入っているなら、以下のコマンドでインストール(グローバル環境)

    npm i -g static

起動は以下のコマンド、 `ctr-c` で終了

    static -p 8000

## リモートサーバとファイルを自動同期する
Webサーバ上のファイルとローカルのファイルを自動同期する仕組みを利用すれば、編集したファイルを保存するだけで、即座にWebページを更新することができます。

### プログラミング用のテキストエディタを使う
多くのデスクトップ/モバイルのプログラミング用のテキストエディタには、サーバ上のファイルを直接編集したりFTP/SFTPでファイルを同期する機能があります（プラグインで同様の機能が提供されているものもあります）。
- Visual Studio Code: [vscode-sftp](https://github.com/liximomo/vscode-sftp)
- Atom: [remote-ftp](https://atom.io/packages/remote-ftp)
- Blackets: [SFtpUpload](https://github.com/bigeyex/brackets-sftp-upload)
- Sublime Text: [SFTP](https://codexns.io/products/sftp_for_sublime)
- Vi, Vim, Emacs...

### ファイル同期ソフトを使う
ディレクトリを監視して変化があったら自動的にバックアップを取るアプリケーションにはFTP/SFTP経由でサーバ上のファイルと同期する機能を持っているものがあります。
- [FreeFileSync: Open Source File Synchronization & Backup Software](https://freefilesync.org/)

### コードホスティングサービスを使う
GitHub、GitLab、Bitbucketといったソースコードのホスティングサービスには、たいてい自動デプロイの仕組みがあるので、それを利用して同期することもできます。でも、こういうサービスがするする使えるなら、そもそもこんなスクリプトは必要ないですよね...(´・ω・`)
- [GitHub](https://github.co.jp/)
- [GitLab](https://gitlab.com/)
- [Bitbucket](https://bitbucket.org/)