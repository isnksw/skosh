# Skosh: API

<div class="toc"></div>

## Basics
実際には、SkoshはWikiではありません。指定されたHTMLの要素内にMarkdownで書かれたファイルをレンダリングして流し込むJavaScriptライブラリです。URLクエリを受け取って要素を更新する機能を使うとWikiっぽく使える、というだけです。ライブラリとしてのSkoshは、シンプルで柔軟性の高いルールと、高い拡張性を持ち、様々な用途に使えます。

ライブラリとしての Skosh の使い方は以下の通りです。

1. skosh.js を読み込む。
2. 任意の id を付加した空要素を必要な位置に挿入する。
3. `Skosh()`を、先に付与した id で初期化。
4. `load()`メソッドに markdown ファイル、もしくはファイル名の配列を渡す。

### サンプル: 最小構成

以下は最小構成の Skosh です。 この例では、`<div id="main"></div>`内に、markdown-it.js でレンダリングされた `index.md` が展開されます。

```html
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8" />
    <title>Skosh</title>
    <script src="./assets/js/skosh.js"></script>
  </head>
  <body>
    <div id="main"></div>
    <script>
      window.onload = function() {
        var skosh = new Skosh("main");
        skosh.load("index.md");
      };
    </script>
  </body>
</html>
```

この状態で、`?main=ファイル名`という URL クエリが使えます。ここで指定している`main`は、`var skosh = new Skosh("main");`で初期化している id です。

つまり、デフォルトの`index.html`は、以下のような html と javascript で初期化されている、というわけです（実際にはこれに加えてプラグインなどの指定が入っています）。

```html
<div id="header"></div>
<div id="main"></div>
<div id="footer"></div>
```

```javascript
var header = new Skosh("header");
header.load("header.md");

var main = new Skosh("main");
main.load("index.md");

var footer = new Skosh("footer");
footer.load("footer.md");
```

### サンプル: リアルタイム更新

ファイルの読み込みと表示は動的に行われているので、ページの中身を書き換えるのに必ずしもリロードは必要ありません。以下の例では、イベントなどの際のリアルタイム更新を想定し、ページ遷移無しで、10 秒ごとに`short_update.md`を読み込んで`main`を更新し、60 秒ごとに`long_update.md`を読み込んで`archive`を更新します。

```html
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8" />
    <title>Skosh</title>
    <script src="./assets/js/skosh.js"></script>
  </head>
  <body>
    <div id="main"></div>
    <div id="archive"></div>
    <script>
      window.onload = function() {
        var main = new Skosh("main");
        main.load("short_update.md");

        var archive = new Skosh("archive");
        archive.load("long_update.md");

        var short_updator = window.setInterval(function() {
          main.load("short_update.md");
        }, 10000);

        var long_updator = window.setInterval(function() {
          archive.load("long_update.md");
        }, 60000);
      };
    </script>
  </body>
</html>
```


### URL クエリでのファイル指定の制限

URL クエリでのファイル指定は、セキュリティ上のリスクを避けるため、異なるディレクトリのファイルを指定できません。

```
index.html?main=./data/index.md
```

上のように指定しても、以下の指定と同じものとして処理します。

```
index.html?main=index.md
```

### データディレクトリの指定

Skosh は初期化されたディレクトリをデータディレクトリとみなします。つまり、以下のように別ディレクトリのファイルで初期化すると、以後、URL クエリで指定されたファイルは、`./data/`以下にあるものとみなします。

```
  var main = new Skosh("main");
  main.load("./data/index.md");

```

上記のように初期化すると、Skosh は以下の指定で`./data/sub_page.md`を表示します。

```
index.html?main=sub_page.md
```

### 初期化オプション

`Skosh()`は、第２引数にハッシュ（連想配列）を渡すことでオプションを指定できます。

以下の例は、全てデフォルトの値が指定されています。`var main = new Skosh("main")`としたときと同じ結果になります。

```javascript
var id = "main";
var main = new Skosh(id, {
  format: "markdown",
  query: true,
  link_target: id,
  sanitize: true,
  html: false
});
```

#### format

読み込むファイルのフォーマット。`raw`を指定すると、Markdownレンダラを通さずに、ファイルの内容をそのまま表示します。デフォルトは`markdown`

#### query

URL クエリでの読み込みを許可するか(`true`)、否か(`false`)。デフォルトは`true`。

#### link_target

link に（外部リンクではなく）ファイルが指定された際の、コンテンツを展開するエレメントの ID です。デフォルトでは、そのリンクが属する領域に展開されます。

```javascript
var header = new Skosh("header", {
  link_target: "main"
});
```

たとえば、上のように指定すると、header 内で`[link](test.md)` と書かれたリンクをクリックすると、 test.md は`main`内に展開されます。

#### sanitize
[DOMPurify](https://github.com/cure53/DOMPurify)によるサニタイズを許可するか(`true`)、否か(`false`)。デフォルトは`true`。

DomPurifyによるサニタイズは、スクリプトの実行につながるようなタグや属性を削除するという形で行われます。そのため、この指定を入れていても一部のHTMLタグはそのまま使うことができます。htmlの記述を排除するためには、htmlオプションを`false`に設定する必要があります。

#### html
markdown内にhtmlを記述することを許可するか(`true`)、否か(`false`)。デフォルトは`true`。

このオプションを`false`に指定すると、HTMLタグをMarkdown内に書くことはできません。

## 名前空間/組み込み関数

### Moskosh（名前空間）
`Moskosh`は、拡張用の名前空間です。スクリプト本体がロードされたときに初期化されています。必ずしも使う必要はありませんが、この名前空間にプラグインを登録することで、プラグイン同士の競合を回避したり、値を共有したりする際にちょっと便利です。

以下のように宣言することで、関数名の重複による誤動作を防ぐことができます。

```javascript
var MoSkosh = MoSkosh || {};
MoSkosh.PlugInName = MoSkosh.PlugInName　|| function (obj) {
  /* do something */
}
```

### MoSkosh.ScriptLoader()
外部JavaScriptをロードします。必ずしも使う必要はありませんが、この関数を通してロードすると、同一URLから重複して取得してしまうのを回避することができ、またスクリプトのロードが終了した時点でコールバックを実行することができます。

```javascript
MoSkosh.ScriptLoader(scripts,callback)
```
`scripts`にはロードする外部スクリプトのURLのリストを配列で渡します。すべてのスクリプトがロードされたあとで`callback`に登録された関数が実行されます。

以下の例ではPixiJSをCDNから読み込み、サンプルを実行しています。

```javascript
const draw_circle = function(){
  const app = new PIXI.Application({ width: 640, height: 360 });
  document.getElementsByTagName("body")[0].appendChild(app.view);
  const circle = new PIXI.Graphics();
  circle.beginFill(0x5cafe2);
  circle.drawCircle(0, 0, 80);
  circle.x = 320;
  circle.y = 180;
  app.stage.addChild(circle);
}
MoSkosh.ScriptLoader([
  "https://cdnjs.cloudflare.com/ajax/libs/pixi.js/5.2.0/pixi.min.js"
],draw_circle) 
```
cf. [PixiJS](https://www.pixijs.com/)

### MoSkosh.StyleLoader()
外部CSSをロードします。必ずしも使う必要はありませんが、この関数を通してロードすると、同一URLから重複して取得してしまうのを回避することができます。また、すべてのコンテンツを表示した後にCSSが再適用されるため、確実にページ内のレイアウトに反映させることができます。

```javascript
MoSkosh.StyleLoader(styles);
```
`styles`にはロードする外部CSSファイルのURLのリストを配列で渡します。

以下の例ではCDNからnormalize.cssとGoogle Fontsからフォントの指定を読み込んでいます。

```javascript
MoSkosh.StyleLoader([
  "https://yarnpkg.com/en/package/normalize.css",
  "https://fonts.googleapis.com/icon?family=Material+Icons"
])
```

## API Hook

Skoshには、以下のタイミングで個々のコンテンツやページ全体の処理に介入するHookが用意されています。

1. Markdownのレンダリングの前(ファイルが読み込まれた直後)
2. ファイル内のコード部分(\`\`\` *CODE* \`\`\`)の処理毎
3. Markdownのレンダリングの後(ターゲットの要素内にコンテンツが表示される直前)
4. ターゲットの要素内にコンテンツが表示された後
5. 全てのコンテンツが表示された後（ページ全体が表示された後）

このうち、1-4は、ファイルが読み込まれ、Markdownがレンダリングされ、コンテンツが要素内に展開されるたびに実行されます。これらの処理は個々のファイルごとに非同期で行われているため、読み込みの順番や要素間の参照などは保証されません。5はページ全体の表示が終わった後で1度だけ実行されます。要素間での参照が必要な場合はこのタイミングで行う必要があります。

### pre_rendering_hook
ここに登録された命令は、個々のファイルが読み込まれ、markdown-it.jsでのレンダリングの**前に**実行されます。結果をmarkdown-it.jsでレンダリングするために、処理後のコンテンツを`return`で返すのを忘れないでください。

```javascript
var main = new Skosh("main");
main.pre_rendering_hook(function(id, contents){
    // do somthing...
    return contents
});
main.load(["index.md"]);
```

### code_highlight_hook
ここに登録された命令は、markdown-it.jsでのコード(\`\`\` *CODE* \`\`\`)のレンダリング時に実行されます。

結果をページに表示するために、処理後のコードを`return`で返すのを忘れないでください。忘れると、次の処理にコンテンツが渡されなくなるため、`<code>`内に何も表示されなくなります。

```javascript
var main = new Skosh("main");
main.code_highlight_hook(function(id, code, lang){
    // do somthing...
    return code
});
main.load(["index.md"]);
```

以下の例は、highlight.jsでのシンタックスハイライトの指定です。

```javascript
var main = new Skosh("main");
main.code_highlight_hook(function(id, code, lang){
    let highlighted_code 
    highlighted_code = hljs.highlightAuto(code, [lang]).value;
    return highlighted_code
});
main.load(["index.md"]);
```

### post_rendering_hook
ここに登録された命令は、markdown-it.jsでのレンダリングが終わり、個々のコンテンツがターゲットの要素内に表示される**前に**実行されます。この時点では、コンテンツはDOM要素ではなく、テキストとして処理する必要があります。DOM要素として処理する必要がある場合は、`post_content_load_hook`を使用してください。

結果をページに表示するために、処理後のコンテンツを`return`で返すのを忘れないでください。忘れると、次の処理にコンテンツが渡されなくなるため、ページに何も表示されません。

```javascript
var main = new Skosh("main");
main.post_rendering_hook(function(id, contents){
    // do somthing...
    return contents
});
main.load(["index.md"]);
```

### post_content_load_hook
ここに登録された命令は、markdown-it.jsでのレンダリングが終わり、コンテンツがターゲットの要素内に表示された**後に**実行されます。レンダリング後のhtmlを操作する必要がある場合は、このHookを使う必要があります。ただし、他の要素にコンテンツが表示されているかどうか（ページ全体の表示が終わっているかどうか）は問いません。要素間をまたいで処理が必要な場合は`post_page_load_hook`を使ってください。

処理後のコンテンツを`return`で返しても何も起きません。要素内のコンテンツを書き換えるためには、明示的に`document.getElementById(id)`を呼ぶ必要があります。

```javascript
var main = new Skosh("main");
main.post_content_load_hook(function(id, contents){
    // do somthing...
    document.getElementById(id) = contents;
});
main.load(["index.md"]);
```

以下は、MathJaxでの数式表示の指定の例です。MathJaxはデフォルトでは。スクリプトの読み込み時にページ内のLaTeXの記述を探してレンダリングしますが、Skoshの場合は動的にコンテンツを生成するため、レンダリング終了時にMathJaxの処理を行わないと、数式が正常に表示されません。ここでは、MathJaxの読み込み時の処理を止め、コンテンツがロードされた後に処理を実行するように設定しています。

```javascript
<script type="text/x-mathjax-config">
    MathJax.Hub.Config({skipStartupTypeset: true});      
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-AMS-MML_HTMLorMML&delayStartupUntil=configured"></script>

<script>
var main = new Skosh("main");
main.post_content_load_hook(function(id,contents){
    MathJax.Hub.Configured();
    var html = document.getElementById(id);
    MathJax.Hub.Queue(["Typeset",MathJax.Hub,html]);
});
main.load(["index.md"]);
</script>
```

### post_page_load_hook
ここに登録された命令は、個々のコンテンツの表示がすべて終わり、ページ全体が表示された**後に**、**1度だけ**実行されます。表示エリアをまたいで処理をする必要がある場合は、このHookを使う必要があります。なお、処理後のコンテンツを`return`で返しても何も起きません。

```javascript
var header = new Skosh("header");
header.load("header.md")

var main = new Skosh("main");
main.load("main.md")
main.post_page_load_hook(function () {
  // do somthing...
};

var footer = new Skosh("footer");
footer.load("main.md")
footer.post_page_load_hook(function () {
  // do somthing...
};
```
上記の例では、`main`と`footer`のインスタンスで2回に分けて登録されていますが、登録されたスクリプトはheader, main, footerの全てのコンテンツが表示された後でまとめて実行されます。ここでは`main`と`footer`のインスタンスとして別々に登録していますが、実際にはどのインスタンスで登録しても同じ結果になります。