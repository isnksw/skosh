# Skosh - A tiny client-side CMS/Wiki system

Skoshは、Markdownで書かれたファイルを読み込んで、レンダリングして、表示するだけのシンプルなCMS/Wikiシステムです。ブラウザ上のJavaScriptだけで動作し、静的なファイルのみで構成されているので、必要なファイルをアップロードするだけで動きます。ローカルにもサーバ側にも何かをインストールしたり、設定したりする必要がありません。 [more...](skosh_about.md)

## [Getting Started](skosh_getting_started.md)
1. システムをダウンロードして、
2. テンプレートを編集して、
3. 必要に応じて文書を追加して、
4. Webサーバにアップすれば、動きます
[more...](skosh_getting_started.md)

## Download
https://gitlab.com/isnksw/skosh/-/archive/master/skosh-master.zip?path=src

## Source Code
https://gitlab.com/isnksw/skosh

## License
&copy; 2019 Isana Kashiwai ([MIT license](https://gitlab.com/isnksw/skosh/blob/master/LICENSE))
