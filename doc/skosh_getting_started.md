# Skosh: Getting Started

TOC{.toc}

## 準備
スクリプトをダウンロードして、必要に応じて文書を編集・追加し、適当なWebサーバにアップロードすれば、そのまま動きます。

なお、動作にはWebサーバが必要です。ダウンロードした`index.html`を直接ブラウザで開いても動作しません。ローカル環境で動かす方法についてはTipsの「簡易サーバを使ってローカルで動作させる」を参照してください。

### ダウンロード
https://gitlab.com/isnksw/skosh/-/archive/master/skosh-master.zip?path=src

### Markdown
SkoshはMarkdown記法でテキストをマークアップできます。標準的なMarkdownにくわえて、脚注、タスクリスト、HTML属性といった拡張記法に対応しています。
具体的な記法については、[Skosh: Markdown CheatSheet](skosh_markdown_cheatsheet.md) を参照してください。

## ファイル構成
デフォルトのファイル構成は以下のようになっています。

```
src
 ├─ header.md ... ヘッダ部分のMarkdownファイル
 ├─ index.md ... 本文エリアのMarkdownファイル
 ├─ footer.md　... フッタのMarkdownファイル
 ├─ sidebar.md　... サイドバーのMarkdownファイル（デフォルトでは不使用）
 ├─ index.html ... 表示用のhtmlファイル（編集の必要はありません）
 └─ assets ... スクリプト本体、CSS、画像ファイルなどが入ったディレクトリ
```

編集が必要なのは、拡張子が`.md`になっている3つのMarkdownファイル(sidebarを有効にした場合は4つ)だけです。見た目に手を入れたくなったら、`assets/css/`の`default.css`を修正すればたいていは事足りるはず。システムの本体である`index.html`や`assets/js/`以下のスクリプトを触る必要は、まずありません。

### ヘッダエリア(header.md)
ページの最上部に読み込まれるコンテンツです。デフォルトでは`header.md`が読み込まれます。`MoSkosh.NavBar`のプラグインが適用されており、以下のようなルールでマークアップされます。

- `#ページタイトル`
ハンバーガーメニューのすぐ右側に表示されます。
- 通常の段落
ヘッダエリア内に均等配置されます
- リスト
ハンバーガーメニューをクリックした際に表示されるメニューとして格納されます。

```
# [Scosh](./)

Menu 01

Menu 02

Menu 03

- Dropdown Menu 01
- Dropdown Menu 02
- Dropdown Menu 03

```

### メインエリア(index.md)
ページ中央部、本文エリアに表示されるコンテンツです。デフォルトではindex.htmlにアクセスすると、`index.md`が表示されます。サイト内リンクをクリックするとこのエリアに新しいコンテンツが読み込まれます（header, footer領域は変化しません）。

```
# Skosh - A tiny client-side CMS/Wiki system

Welcome to Skosh!

## H2 level header

### H3 level header

[Markdown Cheatsheet](markdown.md)
```

### フッタエリア(footer.md)
画面最下部、フッタのエリアに表示されるコンテンツです。デフォルトでは`footer.md`が読み込まれます。ここでは、外部リンクとして、SkoshのGitLabリポジトリが指定されています。

```
Powered by [Skosh](https://gitlab.com/isnksw/skosh) &copy; Isana Kashiwai
```

なお、ここに記載されているコピーライト表示については必ずしも残す必要はありません。

### サイドバー（sidebar.md）
*サイドバーはデフォルトでは非表示になっています。*

デフォルトの状態では、sidebarは表示されません。`index.html`中の以下のコメントアウトを外すと、サイドバーが有効になり、sidebar.mdが読み込まれます。CSSとJSは設定済みです。sidebar.mdには、デフォルトで、タイトル、Homeへのリンク、ページ内リンク（目次）が設定されています。適宜内容を修正してください。

```html
<!--<div id="sidebar"></div>-->
```

## サイト内リンク
通常のMarkdownと同じく、以下のような記述でリンクタグが生成されます。その際、サイト内リンクについては、自動的にSkoshのシステムを通すようにリンクが書き換えられます。

```
[サイト内リンク](skosh_about.md) 
[サイト外リンク](https://gitlab.com/isnksw/skosh) 
```
上記のMarkdownは、以下のように変換されます。

```
<a href="./?main=skosh_about.md">サイト内リンク</a>
<a href="https://gitlab.com/isnksw/skosh">サイト外リンク</a>
```

[サイト内リンク](skosh_about.md) 
[サイト外リンク](https://gitlab.com/isnksw/skosh) 

### 複数ファイルの読み込み
サイト内リンクに、カンマ区切りで複数のファイルを指定すると、コンテンツを連結して単一ページとして表示します。

```
[複数ファイルの読み込み](index.md,skosh_about.md) 
```
上記のように指定すると、以下のようなURLを生成します。

```
<a href="./?main=index.md,skosh_about.md">複数ファイルの読み込み</a>
```

[複数ファイルの読み込み](index.md,skosh_about.md) 

### 異なるディレクトリへのリンク
意図しないファイルがシステムに読み込まれるのを防ぐため、サイト内リンクや、URLの直指定で異なるディレクトリのファイルを指定することはできません。サイト内リンクでは異なるディレクトリのファイルはSkoshのシステムを通さず外部リンクと同じように直接リンクタグを生成します。

```
[異なるディレクトリのファイルへのサイト内リンク](assets/js/skosh.js) 
```

[異なるディレクトリのファイルへのサイト内リンク](assets/js/skosh.js) 

また、URLで直接別ディレクトリのファイルを指定しても、無視されます。

```
index.html?main=assets/js/skosh.js
```
上記のような指定は、ディレクトリの指定が無視され、内部的に以下のように書き換えられます。

```
index.html?main=skosh.js
```

[index.html?main=/assets/js/skosh.js](index.html?main=assets/js/skosh.js)

上記の例ではデフォルトのディレクトリに当該ファイルが存在しないため、エラーになり、何も表示されません。

## Markdown内のHTMLタグ

**不特定多数のユーザーがコンテンツをアップロード/編集できる環境では、セキュリティリスクが増すため、以下の指定を変更することは推奨しません。** {style="color:red"}

Skoshはデフォルトの状態では、[DOMPurify](https://github.com/cure53/DOMPurify)を使って、生成されるHTML上でスクリプトが動作するのを抑制しています。そのため、スクリプトタグを始めとして、一部のタグが動作しません。

`index.html`内の以下の箇所の指定を変えることで、すべてのHTMLタグがMarkdownファイル内で有効になります。SNSや動画サイトなどの埋め込みタグを利用するにはこの指定が必要です。

```javascript
// init main
const main = new Skosh("main", {
  sanitize: "true"
});
```
上記の箇所を以下のように、`"true"`から`"false"`に変更します。

```javascript
// init main
const main = new Skosh("main", {
  sanitize: "false"
});
```

